from django.apps import AppConfig


class DatetimepageConfig(AppConfig):
    name = 'datetimepage'
