from django.apps import AppConfig


class ActivitiespageConfig(AppConfig):
    name = 'activitiespage'
