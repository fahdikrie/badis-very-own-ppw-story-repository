from django.urls import path
from . import views

urlpatterns = [
    path('', views.events, name='event'),
    path('add/<str:nama_event>', views.eventsGuestForm, name='eventGuestsAdd'),
    path('deleteEvent/', views.deleteEvent, name='deleteEvent'),
]