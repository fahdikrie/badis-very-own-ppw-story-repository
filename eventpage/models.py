from django.db import models

# Create your models here.

# Buat class yg objectnya buat planning event2
# > NAMA EVENT
# > WAKTU
# > TEMPAT

class Events(models.Model):
    nama_event = models.CharField(max_length=50, unique=True)
    deskripsi = models.TextField(max_length=250)

    def __str__(self):
        return "{}".format(self.nama_event)

    class Meta:
        verbose_name = 'Event'

class EventGuests(models.Model):
    event_host = models.ForeignKey(Events, on_delete=models.CASCADE)
    guest = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return "{}".format(self.guest)

    class Meta:
        verbose_name = 'Event Guest'