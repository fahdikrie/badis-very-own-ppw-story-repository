from django.urls import path
from . import views

urlpatterns = [
    path('', views.academicScheduleForm, name='academicSchedule'),
    path('assignments/<slug:subject_slug>/', views.academicSinglePostView, name='academicAssignment'),
    path('appointment/', views.regularScheduleForm, name='regularSchedule'),
    path('deleteSchedule/', views.deleteAcademicSchedule, name='deleteSchedule'),
    path('deleteAppointment/', views.deleteRegularSchedule, name='deleteAppointment'),
]