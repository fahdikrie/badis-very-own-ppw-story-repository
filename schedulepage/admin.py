from django.contrib import admin
from .models import AcademicSchedule, RegularSchedule, AcademicAssignment

# Register your models here.

class AcademicScheduleAdmin(admin.ModelAdmin):
    list_display = ('subject', 'subject_slug', 'description','lecturer', 'course_credits', 'semester', 'academic_year', 'classroom')
    prepopulated_fields = {'subject_slug': ('subject',)}

# class AcademicScheduleAdmin(admin.ModelAdmin):
#     list_display = ('subject', 'description','lecturer', 'course_credits', 'semester', 'academic_year', 'classroom')

admin.site.register(AcademicSchedule, AcademicScheduleAdmin)
admin.site.register(RegularSchedule)
admin.site.register(AcademicAssignment)

