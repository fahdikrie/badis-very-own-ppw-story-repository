from django import forms
from . import models

sks_choices = (
    (0, '0'),
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
    (6, '6'),
    (7, '7'),
    (8, '8'),
    (9, '9'),
    (10, '10')
    )

semester_choices = (
    ('Odd Semester', 'Odd Semester'),
    ('Even Semester', 'Even Semester')
)

academic_year_choices = (
    ("2019/2020", '2019/2020'),
    ("2020/2021", '2020/2021'),
    ("2021/2022", '2021/2022'),
    ("2022/2023", '2022/2023')
)

class AcademicForm(forms.ModelForm):
    subject = forms.CharField(label='Subject', widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder': 'write the subject...',
            'style' : 'width: 50%;'
        }, 
    )) 
    description = forms.CharField(label='Description',widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "write the subject's description...",
            'style' : 'width: 50%;'
        }, 
    ))
    lecturer = forms.CharField(label='Lecturer',widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "who's the lecturer...",
            'style' : 'width: 50%;'
        }, 
    ))

    course_credits = forms.IntegerField(label='Course Credit', widget=forms.Select(
        choices=sks_choices,
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : '...',
            'style' : 'width: 50%;'
        }, 
    ))

    semester = forms.CharField(label='Semester', widget=forms.Select(
        choices=semester_choices,
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : '...',
            'style' : 'width: 50%;'
        }, 
    ))
    
    academic_year = forms.CharField(label='Academic Year', widget=forms.Select(
        choices=academic_year_choices,
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : '...',
            'style' : 'width: 50%;'
        }, 
    ))

    classroom = forms.CharField(label='Classroom',widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "where's the classroom...",
            'style' : 'width: 50%;'
        }, 
    ))

    class Meta:
        model = models.AcademicSchedule
        fields = (
            'subject',
            'description',
            'lecturer',
            'course_credits',
            'semester',
            'academic_year',
            'classroom'
            )


class RegularForm(forms.ModelForm):
    activity = forms.CharField(label='Activity', widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder': 'write the activity...',
            'style' : 'width: 50%;'
        }, 
    )) 
    activity_desc = forms.CharField(label='Description',widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "write the activity's description...",
            'style' : 'width: 50%;'
        }, 
    ))
    people = forms.CharField(label='People/Guests',widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "who's in it...",
            'style' : 'width: 50%;'
        }, 
    ))
    location = forms.CharField(label='Location',widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "where would it take place...",
            'style' : 'width: 50%;'
        }, 
    ))



    class Meta:
        model = models.RegularSchedule
        fields = (
            'activity',
            'activity_desc',
            'people',
            'location',
            )