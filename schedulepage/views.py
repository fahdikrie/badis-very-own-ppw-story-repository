from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .models import AcademicSchedule, RegularSchedule, AcademicAssignment
from .forms import AcademicForm, RegularForm

# Create your views here.
def academicScheduleForm(request):
    model = AcademicSchedule.objects.all()

    if request.method == 'POST':
        form = AcademicForm(request.POST)
        if form.is_valid():
            form.save() # versi 1
            # models.AcademicForm(**form.cleaned_data).save() # versi 2
            form = AcademicForm()
            
    else:
        form = AcademicForm()

    model = AcademicSchedule.objects.all()

    return render(request, 'schedulepage-templates/schedule.html', {'form': form, 'model' : model})

def academicSinglePostView(request, subject_slug):
    subject = get_object_or_404(AcademicSchedule, subject_slug=subject_slug)
    slug = subject.subject_slug
    assignments = subject.assignments_per_subject.filter(assignment_subject=subject).order_by('deadline')

    context = {
        'subject' : subject,
        'assignments' : assignments,
    }

    return render(request, 'schedulepage-templates/scheduleassignment.html', context)


def regularScheduleForm(request):
    model = RegularSchedule.objects.all()

    if request.method == 'POST':
        form = RegularForm(request.POST)
        if form.is_valid():
            form.save() # versi 1
            # models.AcademicForm(**form.cleaned_data).save() # versi 2
            form = RegularForm()

    else:
        form = RegularForm()

    return render(request, 'schedulepage-templates/appointment.html', {'form': form, 'model' : model})


def deleteAcademicSchedule(request):
    if request.method == 'POST' and 'id' in request.POST:
        AcademicSchedule.objects.get(id=request.POST['id']).delete()

    return redirect(reverse('academicSchedule'))


def deleteRegularSchedule(request):
    if request.method == 'POST' and 'id' in request.POST:
        RegularSchedule.objects.get(id=request.POST['id']).delete()

    return redirect(reverse('regularSchedule'))
