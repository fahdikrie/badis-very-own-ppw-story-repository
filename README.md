[![coverage report](https://gitlab.com/fahdikrie/badis-very-own-ppw-story-repository/badges/master/coverage.svg)](https://gitlab.com/fahdikrie/badis-very-own-ppw-story-repository/commits/master)
[![pipeline status](https://gitlab.com/fahdikrie/badis-very-own-ppw-story-repository/badges/master/pipeline.svg)](https://gitlab.com/fahdikrie/badis-very-own-ppw-story-repository/commits/master)

**PPW Story 1 – 6**
=====

Name : Fahdii Ajmalal Fikrie <br>
Student ID : 1906398370 <br>
PPW 2nd Term 2019/2020 <br>
[https://badisveryown.herokuapp.com/](https://badisveryown.herokuapp.com/)


<br>

**Story 1 — Intro to Web Programming (HTML/CSS)**
-----

Created a simple web profile using html and css, and then deploy it to heroku while learning how gitlab's CI/CD & pipelining work

Link to App : [Web Profile](https://badisveryown.herokuapp.com/story/)

<br>

**Story 2 — Intro to Web Designing**
-----

Learned how to make a mockup (low & high fidelity ones) using wireframe and figma

Link to Wireframe : [Low Fidelity Protoype](https://wireframe.cc/h1cT8N) <br>
Link to Figma : [High Fidelity Protoype](https://www.figma.com/file/72xmH5nHQgGBMfmt3Da5J6/badisveryown?node-id=0%3A1) <br>
Link to Challenge : [Persona-based Design](https://docs.google.com/document/d/10_0KG3GcEq4MEfbh9aaHF1HEhp5HsuabrLvYq8yDEKI/edit?usp=sharing)

<br>

**Story 3 — HTML Bootstrap**
-----

Level up the web profile game using the well known css framework, Bootstrap

Link to App : [Web Profile using Bootstrap](https://badisveryown.herokuapp.com/) <br>
Link to Challenge : – (submission by gdrive)

<br>

**Story 4 — Django Views & Template**
-----

Created a multi-page website using Django Views, URLS, and Templating. Challenge for this week is to make a web page that shows the current date & time, and also make the url takes argument using Django Dynamic URL Routing. As example, adding 1 to the url (ie. https://badisveryown.herokuapp.com/datetime/1) will return the current datetime added by one hour

Link to Main app : [Landing Page](https://badisveryown.herokuapp.com/) <br>
Link to New page : [Blog Page](https://badisveryown.herokuapp.com/blog/) <br>
Link to Challenge : [Datetime Page](https://badisveryown.herokuapp.com/datetime/) <br>

<br>

**Story 5 — Django Models & Database**
-----

Created a web page that shows my academic schedule, user can submit a schedule through the form and then it will be saved on the models' database. Challenge for this week is to implement a delete button for the created schedule.

Link to App : [Schedule Page](https://badisveryown.herokuapp.com/schedule/) <br>
Link to Challenge : [Delete Button](https://badisveryown.herokuapp.com/schedule/) <br>

<br>

**Story 6 — Django Unit Testing**
-----

Created a web page that serves as an event planner, basically the same with the previous story, the models, and the form. But now it is a Test Driven feature, using Unit Testing method. Challenge for this week is to implement a delete button for the user that are joining the event, using TDD approach.

Link to App : [Event Page](https://badisveryown.herokuapp.com/event/) <br>
Link to Challenge : – (skipped the lab) <br>


